# mvmc-valkyrie

vscode的插件，加入了一些常用的自動補全，及文法高亮

## 自動補全
![geti.gif](https://gitlab.com/MVMC-lab/mvmc-valkyrie/raw/master/assets/image/geti2.gif)

## 文法高亮

### TypeOf 的高亮
![typeof_after](https://gitlab.com/MVMC-lab/mvmc-valkyrie/raw/master/assets/image/typeof_after.png)